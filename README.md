# ContestSapin

Ce projet est un challenge sur 2 jours ou nous devons faire des exercices differents
sur ce que l'on a vu dans l'annee.

## Description

Ce projet sera effectue par **Arthur Bourges et Mathis Doré**, ou nous devons obtenir un beau
et grand sapin de noel code en python en collaboration avec Gitlab. Ainsi que decrire l'installation
d'un routeur Pfsense dans une machine virtuelle.

## Image du Projet

!["SapindeNoel"](https://cdn.discordapp.com/attachments/762594529613840421/789498543605153831/unknown.png?width=408&height=686)

## Installation

Pour ouvrir nos travaux, télécharez PyCharm qui est logiciel pour coder en python, les algos pouront etre ouvert dessus aussi
[Lien vers installation](https://www.jetbrains.com/fr-fr/pycharm/download/#section=windows)

## Contribution

Nous nous sommes repartis le travail, les codes en python et les algorithme on été partagé entre nous ainsi
que le travail en computer fundamental. Arthur ayant eu un probleme avec ses logs gitlab meme en changeant de
mot de passe, nous avons décidé que Mathis ferait les commits d'Arthur en son nom.
